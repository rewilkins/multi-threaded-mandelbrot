#include <fstream>
#include <iostream>
#include <chrono>
#include <vector>
#include <cmath>
#include <utility>
#include <thread>

double mapToReal(int x, int imageWidth, double minR, double maxR){
	double range = maxR - minR;
	return x * (range / imageWidth) + minR;
}

double mapToImaginary(int x, int imageHeight, double minI, double maxI){
	double range = maxI - minI;
	return x * (range / imageHeight) + minI;
}

int findMandelbrot(double cr, double ci, int max_interations){
	int i = 0;
	double zr = 0.0, zi = 0.0;
	while (i < max_interations && zr * zr + zi * zi < 4.0){
		double temp = zr * zr - zi * zi + cr;
		zi = 2.0 * zr * zi + ci;
		zr = temp;
		i++;
	}
	return i;
}

void mod_pixal_color(int n, int &r, int &g, int &b){

	int mod_r = 0;
	int mod_g = 5;
	int mod_b = 10;

	r = (n * mod_r % 256); // Changes the Red color
	g = (n * mod_g % 256); // Changes the Green color
	b = (n * mod_b % 256); // Changes the Blue color
}

void make_Mandelbrot_output(){
	int imageWidth = 1024;
	int imageHeight = 1024;
	int maxN = 1024;
	double minR = -1.5;
	double maxR = 0.7;
	double minI = -1.0;
	double maxI = 1.0;

	std::cout << "Generating Mandelbrot_Image.ppm..." << std::endl;

	std::ofstream fout("Mandelbrot_Image.ppm");
	fout << "P3" << std::endl;
	fout << imageWidth << " " << imageHeight << std::endl;
	fout << "256" << std::endl; // Max value of a pixel R,G,B value...

	for (int y = 0; y < imageHeight; y++){
		for (int x = 0; x < imageWidth; x++){
			double cr = mapToReal(x, imageWidth, minR, maxR);
			double ci = mapToImaginary(y, imageHeight, minI, maxI);

			int n = findMandelbrot(cr, ci, maxN);

			int r = 0;
			int g = 0;
			int b = 0;
			mod_pixal_color(n, r, g, b);
			
			fout << r << " " << g << " " << b << " ";
		}
		fout << std::endl;
	}
	fout.close();
	std::cout << "Finished!" << std::endl;
}

std::vector<std::string> generate_mandel(int start, int end, int imageHeight){
	int maxN = 1024;
	double minR = -1.5;
	double maxR = 0.7;
	double minI = -1.0;
	double maxI = 1.0;

	std::vector<std::string> output;

	for (int y = 0; y < imageHeight; y++){
		for (int x = start; x < end; x++){
			double cr = mapToReal(x, end, minR, maxR);
			double ci = mapToImaginary(y, imageHeight, minI, maxI);

			int n = findMandelbrot(cr, ci, maxN);

			int r = 0;
			int g = 0;
			int b = 0;
			mod_pixal_color(n, r, g, b);
			output.push_back(std::to_string(r) + " " + std::to_string(g) + " " + std::to_string(b));
		}
	}
	return output;
}

void make_Mandelbrot_Serial(){
	int imageWidth = 1024;
	int imageHeight = 1024;
	int maxN = 1024;
	double minR = -1.5;
	double maxR = 0.7;
	double minI = -1.0;
	double maxI = 1.0;

	std::vector<std::string> image = generate_mandel(0, imageWidth, imageHeight);

}

void make_Mandelbrot_Threaded(int thread_count){
	int imageWidth = 1024;
	int imageHeight = 1024;

	if(thread_count >= 1 && thread_count <= 8){
		switch(thread_count){
			case 1: {
				std::thread t1([=](){ generate_mandel(0, imageWidth, imageHeight); });
				t1.join();
				break;
			}
			case 2: {
				std::thread t1([=](){ generate_mandel(0, imageWidth/2, imageHeight); });
				std::thread t2([=](){ generate_mandel((imageWidth/2) + 1, imageWidth, imageHeight); });
				t1.join();
				t2.join();
				break;
			}
			case 3: {
				std::thread t1([=](){ generate_mandel(0, imageWidth/3, imageHeight); });
				std::thread t2([=](){ generate_mandel((imageWidth/3) + 1, (imageWidth/3) * 2, imageHeight); });
				std::thread t3([=](){ generate_mandel(((imageWidth/3) * 2) + 1, imageWidth, imageHeight); });
				t1.join();
				t2.join();
				t3.join();
				break;
			}
			case 4: {
				std::thread t1([=](){ generate_mandel(0, imageWidth/4, imageHeight); });
				std::thread t2([=](){ generate_mandel((imageWidth/4) + 1, (imageWidth/4) * 2, imageHeight); });
				std::thread t3([=](){ generate_mandel(((imageWidth/4) * 2) + 1, (imageWidth/4) * 3, imageHeight); });
				std::thread t4([=](){ generate_mandel(((imageWidth/4) * 3) + 1, imageWidth, imageHeight); });
				t1.join();
				t2.join();
				t3.join();
				t4.join();
				break;
			}
			case 5: {
				std::thread t1([=](){ generate_mandel(0, imageWidth/5, imageHeight); });
				std::thread t2([=](){ generate_mandel((imageWidth/5) + 1, (imageWidth/5) * 2, imageHeight); });
				std::thread t3([=](){ generate_mandel(((imageWidth/5) * 2) + 1, (imageWidth/5) * 3, imageHeight); });
				std::thread t4([=](){ generate_mandel(((imageWidth/5) * 3) + 1, (imageWidth/5) * 4, imageHeight); });
				std::thread t5([=](){ generate_mandel(((imageWidth/5) * 4) + 1, imageWidth, imageHeight); });
				t1.join();
				t2.join();
				t3.join();
				t4.join();
				t5.join();
				break;
			}
			case 6: {
				std::thread t1([=](){ generate_mandel(0, imageWidth/6, imageHeight); });
				std::thread t2([=](){ generate_mandel((imageWidth/6) + 1, (imageWidth/6) * 2, imageHeight); });
				std::thread t3([=](){ generate_mandel(((imageWidth/6) * 2) + 1, (imageWidth/6) * 3, imageHeight); });
				std::thread t4([=](){ generate_mandel(((imageWidth/6) * 3) + 1, (imageWidth/6) * 4, imageHeight); });
				std::thread t5([=](){ generate_mandel(((imageWidth/6) * 4) + 1, (imageWidth/6) * 5, imageHeight); });
				std::thread t6([=](){ generate_mandel(((imageWidth/6) * 5) + 1, imageWidth, imageHeight); });
				t1.join();
				t2.join();
				t3.join();
				t4.join();
				t5.join();
				t6.join();
				break;
			}
			case 7: {
				std::thread t1([=](){ generate_mandel(0, imageWidth/7, imageHeight); });
				std::thread t2([=](){ generate_mandel((imageWidth/7) + 1, (imageWidth/7) * 2, imageHeight); });
				std::thread t3([=](){ generate_mandel(((imageWidth/7) * 2) + 1, (imageWidth/7) * 3, imageHeight); });
				std::thread t4([=](){ generate_mandel(((imageWidth/7) * 3) + 1, (imageWidth/7) * 4, imageHeight); });
				std::thread t5([=](){ generate_mandel(((imageWidth/7) * 4) + 1, (imageWidth/7) * 5, imageHeight); });
				std::thread t6([=](){ generate_mandel(((imageWidth/7) * 5) + 1, (imageWidth/7) * 6, imageHeight); });
				std::thread t7([=](){ generate_mandel(((imageWidth/7) * 6) + 1, imageWidth, imageHeight); });
				t1.join();
				t2.join();
				t3.join();
				t4.join();
				t5.join();
				t6.join();
				t7.join();
				break;
			}
			case 8: {
				std::thread t1([=](){ generate_mandel(0, imageWidth/8, imageHeight); });
				std::thread t2([=](){ generate_mandel((imageWidth/8) + 1, (imageWidth/8) * 2, imageHeight); });
				std::thread t3([=](){ generate_mandel(((imageWidth/8) * 2) + 1, (imageWidth/8) * 3, imageHeight); });
				std::thread t4([=](){ generate_mandel(((imageWidth/8) * 3) + 1, (imageWidth/8) * 4, imageHeight); });
				std::thread t5([=](){ generate_mandel(((imageWidth/8) * 4) + 1, (imageWidth/8) * 5, imageHeight); });
				std::thread t6([=](){ generate_mandel(((imageWidth/8) * 5) + 1, (imageWidth/8) * 6, imageHeight); });
				std::thread t7([=](){ generate_mandel(((imageWidth/8) * 6) + 1, (imageWidth/8) * 7, imageHeight); });
				std::thread t8([=](){ generate_mandel(((imageWidth/8) * 7) + 1, imageWidth, imageHeight); });
				t1.join();
				t2.join();
				t3.join();
				t4.join();
				t5.join();
				t6.join();
				t7.join();
				t8.join();
				break;
			}
		}
	}else{
		std::cout << "\nAborted: Support for 1 - 8 threads only.  ";
		std::cout << thread_count;
		std::cout << " threads were requested.\n" << std::endl;
		return;
	}

}

template <typename F1>
double time_0_Perameter_Function(F1 f){

	std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
	f();
	std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);

	return time_span.count();
}

template <typename F1, typename F2>
double time_1_Perameter_Function(F1 f, F2 perameter1){

	std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
	f(perameter1);
	std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);

	return time_span.count();
}

std::vector<double> timeStats(std::vector<double> v, int n){
	
	double sum = 0;
	for (int i = 0; i < n; i++){
		sum += v[i];
	}
	double avg = sum / n;

	double temp = 0;
	std::vector<double> sums;
	for (int i = 0; i < n; i++){
		temp = (v[i] - avg) * (v[i] - avg);
		sums.push_back(temp);
	}
	double sumation = 0;
	for (int i = 0; i < n; i++){
		sumation += sums[i];
	}
	double stddev = sqrt(sumation / n);

	std::vector<double> output;
	output.push_back(avg);
	output.push_back(stddev);

	return output; // or: return {avg, stddev}
}

std::vector<double> calc_SpeedUp(std::vector<double> serial_set, std::vector<double> threaded_set){
	std::vector<double> output;
	std::vector<double> avg_stddev_collection = timeStats(serial_set, serial_set.size());
	
	for(int i = 0; i < threaded_set.size(); i++){
		output.push_back(avg_stddev_collection[0] / threaded_set[i]);
	}
	return output;
}

void assignment_1(){
	int num_of_serial_runs = 3;
	int num_of_threaded_runs = 8;

	// make_Mandelbrot_output();
	std::vector<double> serial_time_collection;
	std::vector<double> threaded_time_collection;
	
	std::cout << std::endl << std::endl << "Serial:" << std::endl;
	std::cout << "Timing " << num_of_serial_runs << " Image Generations...\n" << std::endl;

	for (int i = 0; i < num_of_serial_runs; i++){
		double time = time_0_Perameter_Function(make_Mandelbrot_Serial);
		std::cout << "Image " << i + 1 << ": " << time << " seconds" << std::endl;
		serial_time_collection.push_back(time);
	}

	std::vector<double> avg_stddev_collection = timeStats(serial_time_collection, serial_time_collection.size());
	std::cout << std::endl;
	std::cout << "Average Time: " << avg_stddev_collection[0] << " seconds" << std::endl;
	std::cout << "Standard Deviation: " << avg_stddev_collection[1] << " seconds" << std::endl;
	std::cout << std::endl;

	std::cout << std::endl << std::endl << "Threaded:" << std::endl;
	std::cout << "Timing " << num_of_threaded_runs << " Image Generations...\n" << std::endl;

	for (int i = 0; i < num_of_threaded_runs; i++){
		double time = time_1_Perameter_Function(make_Mandelbrot_Threaded, i + 1);
		std::cout << "Thread count " << i + 1 << ": " << time << " seconds" << std::endl;
		threaded_time_collection.push_back(time);
	}

	std::cout << std::endl << std::endl << std::endl;
	std::cout << "Computing speedup on the " << num_of_threaded_runs << " Image Generations...\n" << std::endl;
	std::vector<double> speedup_collection = calc_SpeedUp(serial_time_collection, threaded_time_collection);
	for(int i = 0; i < speedup_collection.size(); i++){
		std::cout << "1 vs " << i + 1 << " thread(s): " << speedup_collection[i] << std::endl;
	}
}


int main(){
	double runtime = time_0_Perameter_Function(assignment_1);
	// std::cout << std::endl << std::endl << std::endl << "Overall program runtime: " << runtime  << " seconds" << std::endl;
	std::cout << std::endl << std::endl << std::endl << "Overall program runtime: " << runtime/60  << " minutes" << std::endl;
	std::cout << std::endl << std::endl;
	return 0;
}
